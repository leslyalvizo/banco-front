import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateDenomComponent } from './update-denom.component';

describe('UpdateDenomComponent', () => {
  let component: UpdateDenomComponent;
  let fixture: ComponentFixture<UpdateDenomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateDenomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateDenomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
