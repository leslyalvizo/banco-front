import { UpdateDenomService } from './update-denom.service';
import { BancoModel } from './../model/banco.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-denom',
  templateUrl: './update-denom.component.html',
  styleUrls: ['./update-denom.component.css'],
  providers: [UpdateDenomService]
})
export class UpdateDenomComponent implements OnInit {
  private denom: BancoModel;
  constructor(private updateDenomService: UpdateDenomService, private router: Router) {
    this.denom = new BancoModel();
   }

  ngOnInit() {
  }

  public save(): void {
    this.updateDenomService.update(this.denom).subscribe(res => {
      this.router.navigate(['bancoComponent']);
    });
  }
}
