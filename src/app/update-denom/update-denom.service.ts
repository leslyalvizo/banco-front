import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BancoModel } from '../model/banco.model';

@Injectable()
export class UpdateDenomService {

  constructor(private http:HttpClient) { }

  private headers = new Headers({'Content-Type': 'application/json'});

  public update(denom: BancoModel): Observable<BancoModel> {
    return this.http.put<BancoModel>('http://localhost:8080/denominacion/update', denom);
  }
}
