import { TestBed } from '@angular/core/testing';

import { UpdateDenomService } from './update-denom.service';

describe('UpdateDenomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpdateDenomService = TestBed.get(UpdateDenomService);
    expect(service).toBeTruthy();
  });
});
