import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BancoModel } from '../model/banco.model';

@Injectable()
export class CreateDenomService {

  constructor(private http:HttpClient) { }

  private headers = new Headers({'Content-Type': 'application/json'});

  public save(denom: BancoModel): Observable<BancoModel> {
    return this.http.post<BancoModel>('http://localhost:8080/denominacion/create', denom);
  }
}
