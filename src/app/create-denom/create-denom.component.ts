import { CreateDenomService } from './create-denom.service';
import { BancoModel } from './../model/banco.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-denom',
  templateUrl: './create-denom.component.html',
  styleUrls: ['./create-denom.component.css'],
  providers: [CreateDenomService]
})
export class CreateDenomComponent implements OnInit {
  private denom: BancoModel;
  constructor(private createDenomService: CreateDenomService, private router: Router) {
    this.denom = new BancoModel();
   }

  ngOnInit() {
  }

  public save(): void {
    this.createDenomService.save(this.denom).subscribe(res => {
      this.router.navigate(['bancoComponent']);
    });
  }

}
