import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDenomComponent } from './create-denom.component';

describe('CreateDenomComponent', () => {
  let component: CreateDenomComponent;
  let fixture: ComponentFixture<CreateDenomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDenomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDenomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
