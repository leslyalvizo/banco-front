import { TestBed } from '@angular/core/testing';

import { CreateDenomService } from './create-denom.service';

describe('CreateDenomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreateDenomService = TestBed.get(CreateDenomService);
    expect(service).toBeTruthy();
  });
});
