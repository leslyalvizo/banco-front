import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BancoComponent } from './banco/banco.component';
import { CreateDenomComponent } from './create-denom/create-denom.component';
import { UpdateDenomComponent } from './update-denom/update-denom.component';
import { DeleteDenomComponent } from './delete-denom/delete-denom.component';

@NgModule({
  declarations: [
    AppComponent,
    BancoComponent,
    CreateDenomComponent,
    UpdateDenomComponent,
    DeleteDenomComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
