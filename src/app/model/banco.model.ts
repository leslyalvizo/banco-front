import { ParentEntity } from './parentEntity.model';
export class BancoModel extends ParentEntity{
    public descripcion: string;
    public valor: number;
}