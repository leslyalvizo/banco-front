import { TestBed } from '@angular/core/testing';

import { DeleteDenomService } from './delete-denom.service';

describe('DeleteDenomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeleteDenomService = TestBed.get(DeleteDenomService);
    expect(service).toBeTruthy();
  });
});
