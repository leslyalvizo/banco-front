import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ParentEntity } from '../model/parentEntity.model';

@Injectable({
  providedIn: 'root'
})
export class DeleteDenomService {
  constructor(private http:HttpClient) { }

  private headers = new Headers({'Content-Type': 'application/json'});

  public delete(parent: ParentEntity): Observable<ParentEntity> {
    return this.http.put<ParentEntity>('http://localhost:8080/denominacion/delete/{id}', parent);
  }
}
