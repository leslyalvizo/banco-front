import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteDenomComponent } from './delete-denom.component';

describe('DeleteDenomComponent', () => {
  let component: DeleteDenomComponent;
  let fixture: ComponentFixture<DeleteDenomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteDenomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDenomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
