import { ParentEntity } from './../model/parentEntity.model';
import { Component, OnInit } from '@angular/core';
import { DeleteDenomService } from './delete-denom.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-denom',
  templateUrl: './delete-denom.component.html',
  styleUrls: ['./delete-denom.component.css'],
  providers: [DeleteDenomService]
})
export class DeleteDenomComponent implements OnInit {
  private parent: ParentEntity;
  constructor(private deleteDenomService: DeleteDenomService, private router: Router) {
    this.parent = new ParentEntity();
   }

  ngOnInit() {
  }

  public delete(): void {
    this.deleteDenomService.delete(this.parent).subscribe(res => {
      this.router.navigate(['bancoComponent']);
    });
  }
}
