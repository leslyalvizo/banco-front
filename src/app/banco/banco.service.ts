import { BancoModel } from './../model/banco.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class BancoService {
  constructor(private http:HttpClient) { }

  public getDenom(): Observable<BancoModel[]>{
    return this.http.get<BancoModel[]>("http://localhost:8080/denominacion/getall");
  }
}
