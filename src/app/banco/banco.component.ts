import { BancoModel } from './../model/banco.model';
import { BancoService } from './banco.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-banco',
  templateUrl: './banco.component.html',
  styleUrls: ['./banco.component.css'],
  providers: [BancoService]
})
export class BancoComponent implements OnInit {
  private list : Array<BancoModel>;
  constructor(private bancoService: BancoService) { }

  ngOnInit() {
    this.load();
  }

  private load(): void{
    this.bancoService.getDenom().subscribe(res => {
      this.list = res;
    });
  }

}
