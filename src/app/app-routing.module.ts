import { CreateDenomComponent } from './create-denom/create-denom.component';
import { UpdateDenomComponent } from './update-denom/update-denom.component';
import { DeleteDenomComponent } from './delete-denom/delete-denom.component';
import { BancoComponent } from './banco/banco.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

const routes: Routes = [
    { path: '', redirectTo: '/bancoComponent', pathMatch: 'full'},
    { path: 'appComponent', component: AppComponent },
    { path: 'bancoComponent', component: BancoComponent },
    { path: 'createDenomComponent', component: CreateDenomComponent },
    { path: 'updateDenomComponent', component: UpdateDenomComponent },
    { path: 'deleteDenomComponent', component: DeleteDenomComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})

export class AppRoutingModule {

}
